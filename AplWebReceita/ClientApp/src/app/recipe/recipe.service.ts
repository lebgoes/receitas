import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Ingredient } from '../recipe/Ingredient';

@Injectable()
export class RecipeService {
  private readonly API = '/api/recipe/';

  constructor(private http: HttpClient) { }

  listIngredients() {
    return this.http.get<Ingredient>(this.API)
  }

}

import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-recipe-component',
  templateUrl: './recipe.component.html',
  preserveWhitespaces: true
})
export class RecipeComponent implements OnInit {
  private readonly API = `${environment.API}api/recipe/`;

  ingredients$: Observable<Ingredient[]>;
   
  onSubmit(form) {
    //console.log(form.value);

    this.http.post(this.API + 'receita', JSON.stringify(form.value)).subscribe(dados => console.log(dados));
  }

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.ListIngredients();
  }

  ListIngredients() {
    this.ingredients$ = this.http.get<Ingredient[]>(this.API + 'ingredientes');
  }
}

interface Ingredient {
  codigo: string;
  descr: string;
}

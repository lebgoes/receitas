export interface Ingredient {
  value: string;
  text: string;
}

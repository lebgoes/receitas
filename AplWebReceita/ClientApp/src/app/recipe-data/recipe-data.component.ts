import { Component, Inject, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-recipe-data',
  templateUrl: './recipe-data.component.html',
  preserveWhitespaces: true
})
export class RecipeDataComponent implements OnInit {
  public recipes: Recipe[];

  constructor(private http: HttpClient) { }

  ngOnInit() {
    this.ListRecipes();
  }

  ListRecipes() {
    this.http.get<Recipe[]>('/api/recipe/receitas').subscribe(result => {
      this.recipes = result;
    }, error => console.error(error));
  }
}     

interface Recipe {
  Id: number;
  nome: string;
  porcao: string;
  caloria: string;
}

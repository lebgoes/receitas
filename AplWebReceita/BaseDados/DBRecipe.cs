﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using AplWebReceita.Models;

namespace AplWebReceita.BaseDados
{
    public static class DBRecipe
    {

        public static IQueryable<Recipe> Recipes()
        {
            SqlConnection sqlconnection = DataConexao();

            try
            {
                List<Recipe> recipesList = new List<Recipe>();

                sqlconnection.Open();
                SqlCommand sqlCommand = new SqlCommand("select * from Recipe", sqlconnection);
                SqlDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())
                {
                    Recipe recipe = new Recipe();

                    recipe.Id = int.Parse(dr["Id"].ToString());
                    recipe.Nome = dr["Nome"].ToString();
                    recipe.Caloria = int.Parse(dr["Caloria"].ToString());
                    recipe.Porcao = int.Parse(dr["Porcao"].ToString());

                    recipesList.Add(recipe);
                }

                return recipesList.AsQueryable();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                sqlconnection.Close();
            }
        }

        public static Recipe Recipes(int id)
        {
            Recipe recipe = new Recipe();
          
            SqlConnection sqlconnection = DataConexao();

            try
            {
                sqlconnection.Open();
                SqlCommand sqlCommand = new SqlCommand("select * from Recipe where Id = " + id, sqlconnection);
                SqlDataReader dr = sqlCommand.ExecuteReader();

                if (dr.HasRows)
                {
                    recipe.Id = int.Parse(dr["Id"].ToString());
                    recipe.Nome = dr["Nome"].ToString();
                    recipe.Caloria = int.Parse(dr["Caloria"].ToString());
                    recipe.Porcao = int.Parse(dr["Porcao"].ToString());
                }

                return recipe;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                sqlconnection.Close();
            }
        }

        public static IQueryable<Recipe> Recipes(string ingrediente)
        {
            SqlConnection sqlconnection = DataConexao();

            try
            {
                List<Recipe> recipesList = new List<Recipe>();
                string sSql = string.Format("select Recipe.Id, Recipe.Nome, Recipe.Caloria, Recipe.Porcao, Recipe.Preparo from Recipe inner join Ingrediente on Ingrediente.Id_Recipe = Recipe.Id where Ingrediente.Nome = '{0}'", ingrediente);

                sqlconnection.Open();
                SqlCommand sqlCommand = new SqlCommand(sSql, sqlconnection);
                SqlDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())
                {
                    Recipe recipe = new Recipe();

                    recipe.Id = int.Parse(dr["Id"].ToString());
                    recipe.Nome = dr["Nome"].ToString();
                    recipe.Caloria = int.Parse(dr["Caloria"].ToString());
                    recipe.Porcao = int.Parse(dr["Porcao"].ToString());
                    recipe.Preparo = dr["Preparo"].ToString();

                    recipesList.Add(recipe);
                }

                return recipesList.AsQueryable();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                sqlconnection.Close();
            }
        }

        public static void SaveRecipe(Recipe recipe)
        {
            SqlConnection sqlconnection = DataConexao();

            try
            {
                sqlconnection.Open();

                string sSql = null;
                int iRet = 0;

                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand.Connection = sqlconnection;

                sSql = string.Format("insert into Recipe (Nome,Porcao,Caloria,Preparo) values({0},{1},{2},{3})", recipe.Nome, recipe.Porcao, recipe.Caloria, recipe.Preparo);
                sqlCommand.CommandText = sSql;

                iRet = sqlCommand.ExecuteNonQuery();

                if (iRet > 0)
                {
                    sqlCommand.CommandText = "select Max(id) from recipes";
                    iRet = sqlCommand.ExecuteNonQuery();

                    if (iRet > 0)
                    {
                        foreach (var item in recipe.Ingrediente)
                        {
                            sSql = string.Format("insert into Ingrediente (Nome,Id_Recipe) values({0},{1})", item.Nome, iRet);
                            sqlCommand.CommandText = sSql;
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            finally
            {
                sqlconnection.Close();
            }
        }
    
        public static IQueryable<Ingrediente> Ingredientes(int id)
        {
            SqlConnection sqlconnection = DataConexao();

            try
            {
                List<Ingrediente> ingredientesList = new List<Ingrediente>();

                string sSql = string.Format("select Nome from Ingrediente where Id_Recipe = {0}", id);
                sqlconnection.Open();
                SqlCommand sqlCommand = new SqlCommand(sSql, sqlconnection);
                SqlDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())
                {
                    Ingrediente ingrediente = new Ingrediente();

                    ingrediente.Nome = dr["Nome"].ToString();

                    ingredientesList.Add(ingrediente);
                }

                return ingredientesList.AsQueryable();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                sqlconnection.Close();
            }
        }

        public static IQueryable<Ingrediente> Ingredientes()
        {
            SqlConnection sqlconnection = DataConexao();

            try
            {
                List<Ingrediente> ingredientesList = new List<Ingrediente>();

                string sSql = "select distinct Ingrediente.Nome from Ingrediente inner join Recipe on Recipe.Id = Ingrediente.Id_Recipe";
                sqlconnection.Open();
                SqlCommand sqlCommand = new SqlCommand(sSql, sqlconnection);
                SqlDataReader dr = sqlCommand.ExecuteReader();

                while (dr.Read())
                {
                    Ingrediente ingrediente = new Ingrediente();

                    ingrediente.Nome = dr["Nome"].ToString();

                    ingredientesList.Add(ingrediente);
                }

                return ingredientesList.AsQueryable();
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
            finally
            {
                sqlconnection.Close();
            }
        }

        private static SqlConnection DataConexao()
        {
            string sConnection = ConfigurationManager.AppSetting["ConnectionString:DB_Recipe"];
            return new SqlConnection(sConnection);
        }
    }
}

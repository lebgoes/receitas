using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using AplWebReceita.Models;
using AplWebReceita.BaseDados;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace AplWebReceita.Controllers
{
    [Route("api/[controller]")]
    public class RecipeController : Controller
    {
        //GET - /receitas - (devolve todas as receitas em mem�ria)
        [Route("receitas")]
        [HttpGet]
        public IActionResult getReceita()
        {
            try
            {
               return Ok(DBRecipe.Recipes());
            }
            catch (Exception ex)
            {
                return BadRequest(Json(new { message = "Ocorreu erro busca dos registros!" }));
            }
        }

        //GET - /receitas/{id} - (devolve uma receita em mem�ria por id)
        [Route("receitas/{id}")]
        [HttpGet]
        public IActionResult getReceita(int id)
        {
            try
            {
                Recipe recipe = DBRecipe.Recipes(id);

                if(recipe == null)
                {
                    return NotFound();
                }

                return Ok(recipe);
            }
            catch (Exception)
            {

                return BadRequest(Json(new { message = "Ocorreu erro busca do registro!" }));
            }
        }

        //GET - /receitas/ingredientes/{id} (devolve receitas que contenham o ingrediente definido por id)
        [Route("receitas/ingrediente/{id}")]
        [HttpGet]
        public IActionResult getgetReceitas(string id)
        {
            try
            {
                string sIngrediente = null;

                //List<SelectListItem> items = Ingredientes();
                var items = Ingredientes();

                foreach (var item in items)
                {
                    if (item.Codigo == id)
                    {
                        sIngrediente = item.Descr;
                        break;
                    }
                }

                return Ok(DBRecipe.Recipes(sIngrediente));
            }
            catch (Exception)
            {
                return BadRequest(Json(new { message = "Ocorreu erro busca das Receitas!" }));
            }
        }

        //GET - /receitas/ingrediente -- (devolves todos os ingredientes utilizados em receitas)
        [Route("receitas/ingrediente")]
        [HttpGet]
        public IActionResult getIngredientes()
        {
            try
            {
                return Ok(DBRecipe.Ingredientes());
            }
            catch (Exception)
            {
                return BadRequest(Json(new { message = "Ocorreu erro busca dos Ingredientes!" }));
            }
        }

        //GET - /ingredientes -- (devolve os ingredientes dispon�veis, ordenados alfabeticamente)
        [Route("ingredientes")]
        [HttpGet]
        public IActionResult getIgredientes()
        {
            try
            {
                //    Ingrediente ingrediente = new Ingrediente();

                //    ingrediente.IngredienteList = Ingredientes();

                return Ok(Ingredientes());
            }
            catch (Exception)
            {
                return BadRequest(Json(new { message = "Ocorreu erro busca dos Ingredientes!" }));
            }
        }

        //GET - /receitas/{id}/ingredientes -- (devolve os ingredientes de uma receita)
        [Route("receitas/{id}/ingredientes")]
        [HttpGet]
        public IActionResult getIgredientes(int id)
        {
            try
            {
                return Ok(DBRecipe.Ingredientes(id));
            }
            catch (Exception)
            {
                return BadRequest(Json(new { message = "Ocorreu erro busca dos Ingredientes!" }));
            }
        }

        //POST - /receita (inicializa uma receita)
        [Route("receita")]
        [HttpPost]
        public IActionResult posReceita([FromBody] Recipe recipe)
        {
            try
            {
                DBRecipe.SaveRecipe(recipe);

                return Ok();
            }
            catch (Exception)
            {
                return BadRequest(Json(new { message = "Ocorreu erro na grava��o do registro!" }));
            }
        }

        private static IQueryable<IngredientModel> Ingredientes()
        {
            List<IngredientModel> ingredientModels = new List<IngredientModel>
            {
                new IngredientModel { Codigo = "1", Descr = "Farinha" },
                new IngredientModel { Codigo = "2", Descr = "A�ucar" },
                new IngredientModel { Codigo = "3", Descr = "Manteiga" },
                new IngredientModel { Codigo = "4", Descr = "Ovo" },
                new IngredientModel { Codigo = "5", Descr = "Leite" },
                new IngredientModel { Codigo = "6", Descr = "Oleo" },
                new IngredientModel { Codigo = "7", Descr = "Sal" },
                new IngredientModel { Codigo = "8", Descr = "Fermento" }
            };

            return ingredientModels.AsQueryable();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AplWebReceita.Models
{
    public class Recipe
    {
        [JsonProperty(PropertyName = "id")]
        public int Id { get; set; }

        [JsonProperty(PropertyName = "nome")]
        public string Nome { get; set; }

        [JsonProperty(PropertyName = "caloria")]
        public int Caloria { get; set; }

        [JsonProperty(PropertyName = "porcao")]
        public int Porcao { get; set; }

        [JsonProperty(PropertyName = "preparo")]
        public string Preparo { get; set; }

        public List<Ingrediente> Ingrediente { get; set; }
    }
}

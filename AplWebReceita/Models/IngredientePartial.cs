﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace AplWebReceita.Models
{
    public partial class Ingrediente
    {
        public List<SelectListItem> IngredienteList = new List<SelectListItem>();
    }
}
